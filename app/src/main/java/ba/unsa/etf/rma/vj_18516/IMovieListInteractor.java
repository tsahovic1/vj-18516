package ba.unsa.etf.rma.vj_18516;

import java.util.ArrayList;

public interface IMovieListInteractor {
    ArrayList<Movie> get();
}
