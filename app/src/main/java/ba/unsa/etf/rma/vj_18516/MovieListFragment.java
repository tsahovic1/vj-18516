package ba.unsa.etf.rma.vj_18516;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MovieListFragment extends Fragment {
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
//Ovdje se dodjeljuje layout fragmentu,
//tj. ˇsta ´ce se nalaziti unutar fragmenta
//Ovu liniju ´cemo poslije promijeniti
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public interface OnItemClick {
        void onItemClicked(Movie movie);
    }

}
