package ba.unsa.etf.rma.vj_18516;

public interface IMovieListPresenter {
    void refreshMovies();
}
