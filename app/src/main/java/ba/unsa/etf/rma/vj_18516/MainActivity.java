package ba.unsa.etf.rma.vj_18516;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IMovieListView {

    private Button button;
    private ListView listView;
    private EditText editText;

    private IMovieListPresenter movieListPresenter;

    private MovieListAdapter movieListAdapter;

    public IMovieListPresenter getPresenter() {
        if (movieListPresenter == null) {
            movieListPresenter = new MovieListPresenter(this, this);
        }
        return movieListPresenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*movieListAdapter=new MovieListAdapter(getApplicationContext(), R.layout.list_element, new ArrayList<Movie>());
        listView= (ListView) findViewById(R.id.listView);
        listView.setAdapter(movieListAdapter);
        listView.setOnItemClickListener(listItemClickListener);
        getPresenter().refreshMovies();
        editText = (EditText)findViewById(R.id.editText);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (sharedText != null) {
                    editText.setText(sharedText);
                }
            }
        }*/
        // twoPaneMode je privatni atribut klase Pocetni koji je tipa boolean
// ovu variablu ´cemo koristiti da znamo o kojem layoutu se radi
// ako je twoPaneMode true tada se radi o ˇsirem layoutu (dva fragmenta)
// ako je twoPaneMode false tada se radi o poˇcetnom layoutu (jedan fragment)
        private boolean twoPaneMode=false;
//dohvatanje FragmentManager-a
        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout details = findViewById(R.id.movie_detail);
//slucaj layouta za ˇsiroke ekrane
        if (details != null) {
            twoPaneMode = true;
            MovieDetailFragment detailFragment = (MovieDetailFragment)
                    fragmentManager.findFragmentById(R.id.movie_detail);
//provjerimo da li je fragment detalji ve´c kreiran
            if (detailFragment==null) {
//kreiramo novi fragment FragmentDetalji ukoliko ve´c nije kreiran
                detailFragment = new MovieDetailFragment();
                fragmentManager.beginTransaction().
                        replace(R.id.movie_detail,detailFragment)
                        .commit();
            }
        } else {
            twoPaneMode = false;
        }
//Dodjeljivanje fragmenta MovieListFragment
        Fragment listFragment =
                fragmentManager.findFragmentByTag("list");
//provjerimo da li je ve´c kreiran navedeni fragment
        if (listFragment==null){
//ukoliko nije, kreiramo
            listFragment = new MovieListFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.movies_list,listFragment,"list")
                    .commit();
        }else{
//sluˇcaj kada mijenjamo orijentaciju uredaja
//iz portrait (uspravna) u landscape (vodoravna)
//a u aktivnosti je bio otvoren fragment MovieDetailFragment
//tada je potrebno skinuti MovieDetailFragment sa steka
//kako ne bi bio dodan na mjesto fragmenta MovieListFragment
            fragmentManager.popBackStack(null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

    }

    @Override
    public void setMovies(ArrayList<Movie> movies) {
        movieListAdapter.setMovies(movies);
    }

    @Override
    public void notifyMovieListDataSetChanged() {
        movieListAdapter.notifyDataSetChanged();
    }
}
